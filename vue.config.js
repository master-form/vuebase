/* eslint-disable no-param-reassign,@typescript-eslint/no-var-requires,global-require */
const dotenv = require('dotenv');

const env = ['.env', '.env.development', '.env.development.local'];
env.forEach((path) => dotenv.config({path}));

const port = process.env.PORT || 8000;

module.exports = {
	transpileDependencies: ['vuetify', 'vuex-module-decorators'],

	devServer: {
		// allowedHosts: [process.env.VUE_APP_DOMAIN],
		// hostName: process.env.VUE_APP_DOMAIN,
		historyApiFallback: {
			rewrites: [
				// { from: /^\/manage\/?.*/, to: path.posix.join('/', 'manage.html') },
				// { from: /^\/callback\/?.*/, to: path.posix.join('/', 'callback.html') },
				// { from: /./, to: path.posix.join('/', 'index.html') },
			]
		},
		port,
		proxy: {
			'^/api': {
				target: `https://${process.env.PROXY_API}`,
				changeOrigin: true,
				secure: false,
				pathRewrite: {'^/api': '/api/v1'},
				logLevel: 'debug',
				router: {
					[`${process.env.VUE_APP_DOMAIN}`]: process.env.PROXY_API
				}
			}
		}
	},

	chainWebpack: (config) => {
		config.plugin('define').tap((args) => {
			args[0]['process.env'].VERSION = JSON.stringify(require('./package.json').version);
			return args;
		});
	}
};
