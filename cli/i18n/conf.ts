/* eslint-disable prettier/prettier */
/**
 * @author Dmytro Zataidukh
 * @created_at 11/23/19
 */
export type Ti18nConf = {
  outputDir: string;
  currentLang: string;
  languages: string[];
  parseRegExp: RegExp;
};

export default {
  outputDir: "src/locale",
  currentLang: "en",
  languages: ["uk", "ru", "en"],
  // eslint-disable-next-line no-useless-escape
  parseRegExp: /\$t\(['|"]([\w|\s|\{|\}]+)['|"][,|\)]/
} as Ti18nConf;
