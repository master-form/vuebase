import {Collection, CollectionApiProvider, CollectionFetchResponse} from '@zidadindimon/vue-mc';
import {PostModel} from '@/mc/PostModel';
import {PostDto} from '@/mc/types';
import {HttpService} from '@/services/HttpService';

export class PostCollection extends Collection<PostModel, PostDto> {
	protected model(): {new (): PostModel} {
		return PostModel;
	}

	protected api(): CollectionApiProvider<PostDto> {
		return {
			fetch(): Promise<CollectionFetchResponse<PostDto>> {
				return HttpService.getClient<PostDto>().fetchCollection(
					'https://d077d23d-0b99-45c4-9a74-aa321a485e65.mock.pstmn.io/api/me'
				);
			}
		};
	}
}
