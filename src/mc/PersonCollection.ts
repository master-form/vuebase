import {Collection, CollectionApiProvider, CollectionFetchResponse} from '@zidadindimon/vue-mc';
import {PersonModel} from '@/mc/PersonModel';
import {PersonCollectionFiler, PersonDto} from '@/mc/types';
import {HttpService} from '@/services/HttpService';

export class PersonCollection extends Collection<PersonModel, PersonDto, PersonCollectionFiler> {
	protected model(): {new (): PersonModel} {
		return PersonModel;
	}

	protected api(): CollectionApiProvider<PersonDto, PersonCollectionFiler> {
		return {
			fetch(filter?: PersonCollectionFiler): Promise<CollectionFetchResponse<PersonDto>> {
				return HttpService.getClient<PersonDto>().fetchCollection('/persons', filter);
			}
		};
	}
}
