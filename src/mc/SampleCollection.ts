/* eslint-disable @typescript-eslint/no-explicit-any */
import {Collection} from '@zidadindimon/vue-mc';
import SampleModel from '@/mc/SampleModel';

export default class SampleCollection extends Collection<SampleModel, any> {}
