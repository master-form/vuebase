import {Post, PostDto} from '@/mc/types';
import {Model} from '@zidadindimon/vue-mc';

export class PostModel extends Model<PostDto> implements Post {
	body: string = null;

	id: number = null;

	title: string = null;

	userId: number = null;

	get completed(): boolean {
		return !!(this.id % 2);
	}
}
