import {Model, MutationList} from '@zidadindimon/vue-mc';
import {Person, PersonDto} from '@/mc/types';

export class PersonModel extends Model<PersonDto> implements Person {
	address: string = null;

	birthday: string = null;

	country: string = null;

	email: string = null;

	firstName: string = null;

	gender: 'male' | 'female' = null;

	image: string = null;

	lastname: string = null;

	phone: string = null;

	website: string = null;

	protected mutations(data?: PersonDto): MutationList<Person> {
		if (!Object.keys(data).length) {
			return {};
		}
		return {
			firstName: data.firstname,
			address: `${data.address.city}, ${data.address.street}`,
			country: data.address.country
		};
	}

	get fullName(): string {
		return `${this.firstName} ${this.lastname}`;
	}
}
