/* eslint-disable camelcase */
export interface AddressDto {
	buildingNumber: string;
	city: string;
	country: string;
	county_code: string;
	latitude: number;
	longitude: number;
	street: string;
	streetName: string;
	zipcode: string;
}

export interface PersonDto {
	address: AddressDto;
	birthday: string;
	email: string;
	firstname: string;
	gender: 'male' | 'female';
	image: string;
	lastname: string;
	phone: string;
	website: string;
}

export interface Person {
	birthday: string;
	email: string;
	firstName: string;
	gender: 'male' | 'female';
	image: string;
	lastname: string;
	phone: string;
	website: string;
	country: string;
	address: string;
}

export interface PersonCollectionFiler {
	_gender?: 'male' | 'female';
	_birthday_start: string;
	_birthday_end: string;
}
