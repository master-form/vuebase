/* eslint-disable @typescript-eslint/ban-ts-comment */
import Vue from 'vue';
import Vuex from 'vuex';
import {AppState, RootState} from '@/types/state.types';
import {APP_STORE_MODULE_NAME, AppStore} from '@/store/modules/App.store';
import {getModule} from 'vuex-module-decorators';

Vue.use(Vuex);

const store = new Vuex.Store<RootState>({
	modules: {
		[APP_STORE_MODULE_NAME]: AppStore
	}
});

export default store;

// @ts-ignore
export const AppStoreModule = getModule<AppState>(AppStore, store);
