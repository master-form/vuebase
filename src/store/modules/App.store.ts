import {AppState} from '@/types/state.types';
import {Module, VuexModule} from 'vuex-module-decorators';

export const APP_STORE_MODULE_NAME = 'app';

@Module({
	namespaced: true,
	name: APP_STORE_MODULE_NAME
})
export class AppStore extends VuexModule<AppState> {
	version = process.env.VERSION;
}
