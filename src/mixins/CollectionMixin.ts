/* eslint-disable vue/max-len,@typescript-eslint/no-empty-function,@typescript-eslint/explicit-module-boundary-types */
import {Component, Prop, Vue, Watch} from 'vue-property-decorator';
import {AnyRecord, Collection} from '@zidadindimon/vue-mc';
import {Exception} from '@/exceptions';

export const CollectionMixin = <
	T extends Collection<AnyRecord, AnyRecord, AnyRecord, AnyRecord>,
	F = AnyRecord
>(collection: {
	new (): T;
}) => {
	@Component({})
	class MyCollectionMixin extends Vue {
		@Prop({type: Object, default: () => ({})}) readonly filters: F;

		collection: T = new (collection as {new (): T})();

		@Watch('filters', {immediate: true})
		async fetch() {
			try {
				await this.collection.fetch(this.filters);
			} catch (error) {
				await this.onError(error);
			}
		}

		// eslint-disable-next-line @typescript-eslint/no-unused-vars
		async onError(error: Exception) {
			console.error(error);
		}
	}

	return MyCollectionMixin;
};
