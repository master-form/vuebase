/* eslint-disable @typescript-eslint/explicit-module-boundary-types,@typescript-eslint/no-empty-function */
import {Component, Prop, Vue, Watch} from 'vue-property-decorator';
import {AnyRecord, Model} from '@zidadindimon/vue-mc';
import {Exception} from '@/exceptions';

export const ModelMixin = <T extends Model, F = AnyRecord>(model: {new (): T}) => {
	@Component({})
	class Form extends Vue {
		@Prop({type: Object, default: () => ({})}) readonly filters: F;

		model: T = new (model as {new (): T})();

		get attrsError() {
			return this.model.errors.attrs || {};
		}

		get rules() {
			return this.model.rules();
		}

		@Watch('filters', {immediate: true})
		async fetch() {
			if (!Object.keys(this.filters).length) return;
			try {
				await this.model.fetch(this.filters, true);
			} catch (error) {
				await this.onError(error, 'fetch');
				// past you code here
			}
		}

		async save() {
			try {
				await this.model.save();
				this.onSave();
			} catch (error) {
				await this.onError(error, 'save');
			}
		}

		protected onSave() {}

		// eslint-disable-next-line @typescript-eslint/no-unused-vars
		async onError(error: Exception, type?: 'save' | 'fetch') {}
	}

	return Form;
};
