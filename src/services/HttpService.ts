/* eslint-disable @typescript-eslint/no-explicit-any,implicit-arrow-linebreak */
import {AnyRecord, CollectionFetchResponse} from '@zidadindimon/vue-mc';
import {CollectionApiResponse, CollectionResponse} from '@/types/Responce.types';
import axios, {AxiosRequestConfig} from 'axios';
import {httpExceptionFactory} from '@/exceptions';

const axiosResponseErrorInterceptor = (error: any) => {
	// transform response error here
	const {status} = error.response || {status: null};
	const {code, message} = error.toJSON();
	const {error: exception} = error.response?.data || {error: {message}};
	// Any status codes that falls outside the range of 2xx cause this function to trigger
	// Do something with response error
	throw httpExceptionFactory(exception.message, status, code);
};

axios.interceptors.response.use((response) => response, axiosResponseErrorInterceptor);

export class HttpService<R, D = AnyRecord> {
	private config: AxiosRequestConfig = {
		withCredentials: true,
		baseURL: process.env.VUE_APP_BASE_URL
	};

	// eslint-disable-next-line no-shadow
	static getClient<R, D = AnyRecord>(): HttpService<R, D> {
		return new HttpService<R, D>();
	}

	async post(path: string, payload: D = null): Promise<R> {
		const {data} = await axios.post<R>(path, payload, this.config);
		return data;
	}

	// eslint-disable-next-line @typescript-eslint/no-explicit-any
	async fetch(path: string, params: D = {} as any): Promise<R> {
		const {data} = await axios.get<R>(path, {...this.config, params});
		return data;
	}

	async fetchCollection<Meta = any>(path: string, params: D = {} as any): Promise<CollectionFetchResponse<R, Meta>> {
		const {data} = await axios.get<CollectionResponse<R>>(path, {...this.config, params});

		if (Array.isArray(data)) {
			return {
				content: data as R[],
				pages: 1,
				page: 1,
				data: {} as Meta
			};
		}

		const {data: content, meta} = data as CollectionApiResponse<R>;

		const {page = 1, pages = 1, count = content.length, pageSize = 0} = meta || {};
		return {
			content,
			pages,
			page,
			size: pageSize,
			total: count,
			data: {} as Meta
		};
	}
}
