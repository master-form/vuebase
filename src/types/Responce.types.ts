export interface CollectionApiResponse<T> {
	data: T[];
	meta?: {
		page: number;
		pages: number;
		count: number;
		pageSize: number;
	};
}

export interface CollectionFilterOptions {
	page?: number;
	size?: number;
}

export type CollectionResponse<T> = T[] | CollectionApiResponse<T>;
