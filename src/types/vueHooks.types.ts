export interface OnComponentCreate {
	created(): void;
}

export interface OnComponentMount {
	mounted(): void;
}

export interface OnComponentUpdate {
	updated(): void;
}

export interface BeforeComponentCreate {
	beforeCreate(): void;
}

export interface BeforeComponentMount {
	beforeMount(): void;
}

export interface BeforeComponentUpdate {
	beforeUpdate(): void;
}

export interface OnComponentDestroy {
	destroyed(): void;
}

export interface BeforeComponentDestroy {
	beforeDestroy(): void;
}
