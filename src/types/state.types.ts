export interface AppState {
	version: string;
}

export interface RootState {
	app: AppState;
}
