import App from './App.vue';
import './registerServiceWorker';
import router from './router';
import store from './store';
import Vue from 'vue';
import vuetify from './plugins/vuetify';
import {HttpService} from '@/services/HttpService';

Vue.config.productionTip = false;

// eslint-disable-next-line @typescript-eslint/ban-ts-comment
// @ts-ignore
// eslint-disable-next-line no-underscore-dangle
window.__httpService = HttpService.getClient();

new Vue({
	router,
	store,
	vuetify,
	render: (h) => h(App)
}).$mount('#app');
