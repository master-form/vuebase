/* eslint-disable no-shadow */
// eslint-disable-next-line @typescript-eslint/no-unused-vars
import {VuetifyLocale} from 'vuetify/types/services/lang';

declare module 'vue/types/vue' {
	// 3. Declare augmentation for Vue
	interface Vue {
		$currentLang: string;
		$t(key: string, ...params: (string | number)[]);
		$addLocale(lang: string, translates: VuetifyLocale): void;
		$setLocale(lang: string): void;
	}
}
