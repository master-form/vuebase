/* eslint-disable func-names */
import Vue from 'vue';
import Vuetify from 'vuetify/lib';
import {VuetifyLocale} from 'vuetify/types/services/lang';
import vRu from 'vuetify/src/locale/ru';
import vUk from 'vuetify/src/locale/uk';
import ru from '../locale/ru.json';
import uk from '../locale/uk.json';

Vue.use(Vuetify);

const vuetify = new Vuetify({
	icons: {
		iconfont: 'mdi'
	},
	lang: {
		current: 'ru',
		locales: {
			ru: {...vRu, ...ru},
			uk: {...vUk, ...uk}
		}
	}
});

Vue.prototype.$t = function (key: string, ...params: Array<string | number>): string {
	return this.$vuetify.lang.t(`$vuetify.${key}`, ...params);
};

Vue.prototype.$addLocale = async function (lang: string, translates: VuetifyLocale) {
	this.$set(vuetify.framework.lang.locales, lang, translates);
};

Object.defineProperty(Vue.prototype, '$currentLang', {
	get(): string {
		return vuetify.framework.lang.current;
	},
	set(lang: string): void {
		this.$set(vuetify.framework.lang, 'current', lang);
	}
});

export default vuetify;
