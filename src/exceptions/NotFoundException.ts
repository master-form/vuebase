import {HttpException} from '@/exceptions/HttpException';

export class NotFoundException extends HttpException {
	constructor(readonly message: string, readonly code?: number) {
		super(message, 404, code);
	}
}
