import {HttpException} from '@/exceptions/HttpException';

export class UnauthorizedException extends HttpException {
	constructor(readonly message: string, readonly code?: number) {
		super(message, 401, code);
	}
}
