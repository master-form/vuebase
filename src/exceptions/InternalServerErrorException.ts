import {HttpException} from '@/exceptions/HttpException';

export class InternalServerErrorException extends HttpException {
	constructor(readonly message: string, readonly code?: number) {
		super(message, 500, code);
	}
}
