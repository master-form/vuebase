import {HttpException} from '@/exceptions/HttpException';

export class BadRequestException extends HttpException {
	constructor(readonly message: string, readonly code?: number) {
		super(message, 400, code);
	}
}
