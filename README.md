# Vue Boilerplate

[![Vue](https://img.shields.io/badge/Vue-%3E2.6.11-green)](https://vuejs.org/v2/guide/)
[![Vuetify](https://img.shields.io/badge/Vuetify-%3E2.4.10-green)](https://vuetifyjs.com/en/)
[![State manager](https://img.shields.io/badge/Vuex--module--decorators-%3E1.0.1-green)](https://www.npmjs.com/package/vuex-module-decorators)
[![Node dependency](https://img.shields.io/badge/node-~14.5.5-green)]()
[![](https://img.shields.io/badge/TypeScript-~4.1.5-blue)]()


## Project description

This repository is a template project based on `VueJs`, `Vuetify` and `Typescript`. Used as a template `pug`.
Another feature of this project is the use of the library "`@zidadindimon/vue-mc`", for better code design.

### Use `Model` and `Collection`.

[Guide `@zidadindimon/vue-mc`](https://www.npmjs.com/package/@zidadindimon/vue-mc)

Write model and collection like as `PersonCollection.ts` `PersonModel.ts`.

Simple list component 

```vue
// PersonList.vue
<template lang="pug">
  v-list
    person-list-item(v-for="(item, index) in collection" :model="item" :key="`person-${index}`")
</template>

<script lang="ts">
import {Component, Mixins} from 'vue-property-decorator';
import {PersonCollection, PersonCollectionFiler} from '@/mc';
import {CollectionMixin} from '@/mixins/CollectionMixin';
import PersonListItem from '@/components/person/PersonListItem.vue';

@Component({
	components: {
		PersonListItem
	}
})
export default class PersonList extends Mixins(
	CollectionMixin<PersonCollection, PersonCollectionFiler>(PersonCollection)
) {}

</script>

// PersonListItem.vue
<template lang="pug">
	v-list-item
		v-list-item-avatar
			v-img(:src="model.image")
		v-list-item-content
			v-list-item-title {{model.firstName}} ({{model.gender}})
			v-list-item-subtitle {{model.address}}
</template>

<script lang="ts">
import {Component, Prop, Vue} from 'vue-property-decorator';
import {PersonModel} from '@/mc';

@Component({})
export default class PersonListItem extends Vue {
	@Prop({type: PersonModel, required: true}) readonly model: PersonModel;
}
</script>
```

If you use **Model** as **Form** you can extends `ModelMixin.ts` and used like as `CollectionMixin.ts` 

### Use HttpService

**HttpService** is a wrapper on **axios** with some configurations (You can change this configuration). 
This service implement base method that you can use when configure **apiProvider** for `collection` or `model`

Also, HttpService add interceptor to axios that catch response errors and transform to relevant exceptions instance like 
as `BadRequestException` and so else...  

### Store
Used `vuex-module-decorators` as wrapper on `Vuex`   
[See docs](https://championswimmer.in/vuex-module-decorators/)

### Translation

Wse: `$t('message')` in templates or `this.$t('message')` in code;

Run cli command `npm run cli:i18n` for create `{locale}.json` file messages and auto translate empty message using api 
of service https://mymemory.translated.net/.

## CLI

### Project setup
```
npm install
```

### Compiles and hot-reloads for development
```
npm run serve
```

### Compiles and minifies for production
```
npm run build
```

### Run your unit tests
```
npm run test:unit
```

### Lints and fixes files
```
npm run lint
```

### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).