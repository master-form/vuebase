module.exports = {
	env: {
		browser: true,
		es2021: true,
		node: true
	},
	extends: [
		'plugin:vue/essential',
		'@vue/typescript',
		'airbnb-base',
		'plugin:import/typescript',
		'plugin:@typescript-eslint/recommended',
		'plugin:prettier/recommended'
	],
	parser: 'vue-eslint-parser',
	parserOptions: {
		ecmaVersion: 2020,
		parser: '@typescript-eslint/parser',
		sourceType: 'module'
	},
	plugins: ['vue', '@typescript-eslint', 'jest'],
	rules: {
		'no-console': process.env.NODE_ENV === 'production' ? ['error', {allow: ['error']}] : 'off',
		'no-debugger': process.env.NODE_ENV === 'production' ? 'error' : 'off',
		'import/no-unresolved': 'off',
		'import/no-extraneous-dependencies': 'off',
		'no-empty': 'off',
		'max-len': ['error', {code: 150}],
		'vue/max-len': [
			'error',
			{
				code: 120,
				template: 150
			}
		],
		'import/prefer-default-export': 'off',
		'class-methods-use-this': 'off',
		'prefer-destructuring': [
			'error',
			{
				VariableDeclarator: {
					array: false,
					object: true
				},
				AssignmentExpression: {
					array: true,
					object: true
				}
			},
			{
				enforceForRenamedProperties: false
			}
		],
		'import/extensions': [
			'error',
			'ignorePackages',
			{
				js: 'never',
				jsx: 'never',
				ts: 'never',
				tsx: 'never'
			}
		],
		'import/order': 'off'
	},
	settings: {
		'import/resolver': {
			'eslint-import-resolver-custom-alias': {
				alias: {
					'@': './src/*'
				},
				extensions: ['.ts', '.js', '.vue']
			}
		}
	},

	overrides: [
		{
			files: ['**/__tests__/*.{j,t}s?(x)', '**/tests/unit/**/*.spec.{j,t}s?(x)'],
			env: {
				jest: true
			}
		}
	]
};
